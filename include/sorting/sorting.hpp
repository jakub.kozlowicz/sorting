#ifndef SORTING_HPP_
#define SORTING_HPP_

#include <vector>

void bubbleSort(std::vector<int>& table);
void insertSort(std::vector<int>& table);
void heapSort(std::vector<int>& table);
void quickSort(std::vector<int>& table);
void mergeSort(std::vector<int>& table);
void bucketSort(std::vector<int>& table);

#endif /* SORTING_HPP_ */
