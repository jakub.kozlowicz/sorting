#include "sorting/sorting.hpp"

void bubbleSort(std::vector<int>& table) {}

void insertSort(std::vector<int>& table)
{
    int takenValue, j;

    for(int i = 1; i < table.size(); ++i)
    {
        // Take reference value to compare with
        takenValue = table[i];

        // Move elements that are greater than takenValue to
        // position before of their current position
        for(j = i; (j > 0 && table[j - 1] > takenValue); --j)
        {
            table[j] = table[j - 1];
        }

        // Insert reference value to their right place
        table[j] = takenValue;
    }
}

void heapSort(std::vector<int>& table) {}

void quickSort(std::vector<int>& table, int left, int right)
{
    int leftIndex = left, rightIndex = right;

    // Create pivot element to compare with
    // To improve algorithm performance comment line below
    // and uncomment line 37. This is for n^2 complexity
    int pivot = table[left];

    // int pivot = table[left + (right - left) / 2];

    while(leftIndex <= rightIndex)
    {
        // Compare items with created pivot
        while(table[leftIndex] < pivot) ++leftIndex;
        while(table[rightIndex] > pivot) --rightIndex;

        // Swap bigger and smaller value
        if(leftIndex <= rightIndex)
        {
            std::swap(table[leftIndex], table[rightIndex]);
            ++leftIndex;
            --rightIndex;
        }
    }

    // Recursively call left and right subarrays
    if(left < rightIndex) quickSort(table, left, rightIndex);
    if(right > leftIndex) quickSort(table, leftIndex, right);
}

void quickSort(std::vector<int>& table)
{
    quickSort(table, 0, table.size() - 1);
}

void merge(std::vector<int>& table, int left, int pivot, int right)
{
    // Create two subarrays
    int size1 = pivot - left + 1;
    int size2 = right - pivot;
    std::vector<int> leftTable(size1), rightTable(size2);

    // Assign them proper values
    for(int i = 0; i < size1; ++i) leftTable[i] = table[left + i];
    for(int j = 0; j < size2; ++j) rightTable[j] = table[pivot + 1 + j];

    // Start from beggining of every array
    int i = 0, j = 0, k = left;

    // Until end of one subarray pick smaller element and
    // place them in correct position in main array
    for(; i < size1 && j < size2; ++k)
    {
        if(leftTable[i] <= rightTable[j])
        {
            table[k] = leftTable[i];
            ++i;
        }
        else
        {
            table[k] = rightTable[j];
            ++j;
        }
    }

    // When elements in one of subarrays end, fill main
    // array with rest of elements from other subarray
    for(; i < size1; ++i, ++k) table[k] = leftTable[i];
    for(; j < size2; ++j, ++k) table[k] = rightTable[j];
}

void mergeSort(std::vector<int>& table, int left, int right)
{
    // Find element where array will be divided
    int middle = left + (right - left) / 2;

    if(left < right)
    {
        // Sort left side of array
        mergeSort(table, left, middle);

        // Sort right side of array
        mergeSort(table, middle + 1, right);

        // Merge sorted arrays
        merge(table, left, middle, right);
    }
}

void mergeSort(std::vector<int>& table)
{
    mergeSort(table, 0, table.size() - 1);
}

void bucketSort(std::vector<int>& table) {}
