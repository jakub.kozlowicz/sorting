#include "sorting/sorting.hpp"
#include "timer.hpp"

#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <thread>
#include <vector>

using namespace std::chrono_literals;

const int REPEAT = 25;
const int TABLES_SIZE_NUMBER = 20;
const int N2_INCREMENT = 2000;
const int LOG_INCREMENT = 50000;

int randomize();
std::array<int, TABLES_SIZE_NUMBER> generateSizes(int minSize, const int& increment);
std::array<std::vector<int>, TABLES_SIZE_NUMBER> generateTables(const std::array<int, TABLES_SIZE_NUMBER>& tablesSize,
                                                                std::string type);
void testSorting(std::ofstream& stream, const std::array<int, TABLES_SIZE_NUMBER>& size, std::string type,
                 void (*f)(std::vector<int>&));

int main(int argc, char* argv[])
{
    std::ofstream out_quickSort1("../results/quickSort_log.csv");
    std::ofstream out_quickSort2("../results/quickSort_n2.csv");
    std::ofstream out_mergeSort("../results/mergeSort.csv");
    std::ofstream out_insertSort("../results/insertSort.csv");

    // Different values each time program runs
    srand(time(NULL));

    std::cout << "Testing all algorithms simultaneously...\n";
    auto nlognSizes = generateSizes(100000, LOG_INCREMENT);
    auto n2Sizes = generateSizes(1000, N2_INCREMENT);

    std::thread t1 = std::thread(testSorting, std::ref(out_insertSort), std::ref(n2Sizes), std::string("random"),
                                 static_cast<void (*)(std::vector<int>&)>(insertSort));
    std::thread t2 = std::thread(testSorting, std::ref(out_quickSort2), std::ref(n2Sizes), std::string("inverse"),
                                 static_cast<void (*)(std::vector<int>&)>(quickSort));
    std::thread t3 = std::thread(testSorting, std::ref(out_mergeSort), std::ref(nlognSizes), std::string("random"),
                                 static_cast<void (*)(std::vector<int>&)>(mergeSort));
    std::thread t4 = std::thread(testSorting, std::ref(out_quickSort1), std::ref(nlognSizes), std::string("random"),
                                 static_cast<void (*)(std::vector<int>&)>(quickSort));

    t1.join();
    t2.join();
    t3.join();
    t4.join();

    std::cout << "Done.\n";
    return 0;
}

int randomize()
{
    return rand() % 5000000;
}

std::array<int, TABLES_SIZE_NUMBER> generateSizes(int minSize, const int& increment)
{
    std::array<int, TABLES_SIZE_NUMBER> sizeTable;
    for(size_t i = 0; i < TABLES_SIZE_NUMBER; i++)
    {
        sizeTable[i] = minSize;
        minSize += increment;
    }

    return sizeTable;
}

std::array<std::vector<int>, TABLES_SIZE_NUMBER> generateTables(const std::array<int, TABLES_SIZE_NUMBER>& tablesSize,
                                                                std::string type)
{
    std::array<std::vector<int>, TABLES_SIZE_NUMBER> tables;

    for(size_t i = 0; i < TABLES_SIZE_NUMBER; ++i)
    {
        std::vector<int> table(tablesSize[i]);
        std::generate(table.begin(), table.end(), randomize);
        tables[i] = table;
    }

    if(type == "inverse")
    {
        for(size_t i = 0; i < tables.size(); ++i)
        {
            std::sort(tables[i].rbegin(), tables[i].rend());
        }

        return tables;
    }
    else
    {
        return tables;
    }
}

void testSorting(std::ofstream& stream, const std::array<int, TABLES_SIZE_NUMBER>& size, std::string type,
                 void (*f)(std::vector<int>&))
{
    Timer timer;

    for(size_t i = 0; i < REPEAT; ++i)
    {
        auto table = generateTables(size, type);

        for(size_t j = 0; j < table.size(); ++j)
        {
            timer.start();
            (*f)(table[j]);
            timer.stop();
            stream << table[j].size() << ", " << timer.sInterval() << "\n";
        }
    }

    stream.close();
}